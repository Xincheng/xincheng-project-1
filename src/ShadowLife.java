import bagel.*;

public class ShadowLife extends AbstractGame {
    private Image gatherer;
    private Image tree;
    private Image background;
    private String[] nameList;
    private int[] xCoordList;
    private int[] yCoordList;
    private ReadCSV file;
    private long time;
    private int height;
    private int width;

    // constructor for ShadowLife
    public ShadowLife() {
        super(1024, 768, "Gather!");
        gatherer = new Image("res/images/gatherer.png");
        tree = new Image("res/images/tree.png");
        background = new Image("res/images/background.png");
        ReadCSV readFile = new ReadCSV("res/worlds/test.csv");
        height = Window.getHeight();
        width = Window.getWidth();
        this.xCoordList = readFile.getXCoords();
        this.yCoordList = readFile.getYCoords();
        this.nameList = readFile.getNames();
        file = readFile;
        time = System.currentTimeMillis();
    }

    public static void main(String[] args) {
        ShadowLife game = new ShadowLife();
        game.run();
    }

    // update the simulation
    public void update(Input input) {
        boolean moved = false;

        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }

        // draw background and actors
        background.drawFromTopLeft(0, 0);
        for (int i = 0; i < file.getLineNum(); i++) {
            if (nameList[i].equals("Tree")) {
                tree.draw(xCoordList[i], yCoordList[i]);
            }
            if (nameList[i].equals("Gatherer")) {
                // move for every 1 tick
                moved = false;
                // 1 tick cannot be exactly 500, so after testing, choose the range 460-500
                if (System.currentTimeMillis() - time > 460 &&
                        System.currentTimeMillis() - time < 500) {
                    randomMove(i);
                    moved = true;
                }
                gatherer.draw(xCoordList[i], yCoordList[i]);
            }
        }
        if (moved) {
            time = System.currentTimeMillis();
        }
    }

    // method used for the random move of gatherers
    public void randomMove(int index) {
        // choose a random direction and each direction has 25% probability to occur
        double direction = Math.random();
        // sort the directions, 0-0.25 for right, 0.25-0.5 for up, 0.5-0.75 for down and 0.75-1 for left
        if (direction < 0.25) {
            xCoordList[index] += (width / 64);
        } else if (direction > 0.75) {
            xCoordList[index] -= (width / 64);
        } else if (direction > 0.25 && direction < 0.5) {
            yCoordList[index] += (height / 64);
        } else {
            yCoordList[index] -= (height / 64);
        }
    }
}
