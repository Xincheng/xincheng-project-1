import java.io.*;

public class ReadCSV {
    private String file;
    private String[] nameList;
    private int[] xCoordList;
    private int[] yCoordList;
    private static int lineNum;

    // constructor for this class
    public ReadCSV(String file) {
        this.file = file;
        lineNum = countLine();
        nameList = new String[lineNum];
        xCoordList = new int[lineNum];
        yCoordList = new int[lineNum];
        recordLine();
    }

    // getters for this class
    public String[] getNames() { return nameList; }
    public int[] getXCoords() { return xCoordList; }
    public int[] getYCoords() { return yCoordList; }
    public int getLineNum() { return lineNum; }

    // method used for counting the number of lines
    public int countLine() {
        int count = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String text;
            while ((text = br.readLine()) != null) {
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    // method uesd for recording the data in each line separately
    public void recordLine() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String text;
            int count = 0;
            while ((text = br.readLine()) != null) {
                String cells[] = text.split(",");
                nameList[count] = cells[0];
                xCoordList[count] = Integer.parseInt(cells[1]);
                yCoordList[count] = Integer.parseInt(cells[2]);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}